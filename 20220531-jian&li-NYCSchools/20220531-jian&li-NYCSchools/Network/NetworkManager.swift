//
//  NetworkManager.swift
//  20220531-jian&li-NYCSchools
//
//  Created by jianli on 5/31/22.
//

import Foundation

class NetworkManager{
    
    static public func downloadListSchoolData() async throws -> Data{
        guard let url = URL(string:URLs.strListSchool.string)
        else{throw NSError(domain: "bad url:\(URLs.strListSchool.string)", code: -1)}
//        return try Data(contentsOf: url)
        let (data,_) = try await URLSession.shared.data(from:url)
        return data
    }
    
    static public func downloadSATSchoolData(completionHandler:@escaping  (Data?)->Void) throws{
        guard let url = URL(string: URLs.strSATTotal.string)
        else{throw NSError(domain: "bad url:\(URLs.strSATTotal.string)", code: -1)}
//        return try Data(contentsOf: url)
        URLSession.shared.dataTask(with:url){data,res,e in
            completionHandler(data)
        }.resume()
        
    
    }
}
