//
//  SchoolListViewModel.swift
//  20220531-jian&li-NYCSchools
//
//  Created by jianli on 5/31/22.
//

import Foundation

class SchoolListViewModel:ObservableObject{
    // schools catch
    @Published var schools = [SchoolModel]()
    // sats catch
    @Published var sats = [SchoolSATModel]()
    
    @MainActor
    init(){
        //var data = Data()
        Task{
        do {
            let data = try await NetworkManager.downloadListSchoolData()
            self.schools = try JSONDecoder().decode([SchoolModel].self, from: data)
            }catch(let e){
                print(e.localizedDescription)
            }
        }
        do {
            let data = try NetworkManager.downloadSATSchoolData{data in
                self.loadSATData(data: data)
            }
            print("load data success!")
        }catch(let e){
            print(e)
        }
        
    }
    
    init(data:Data){
        do{
            self.schools = try JSONDecoder().decode([SchoolModel].self, from: data)
        }catch(let e){
            print(e)
        }
    }
    
    func loadSATData(data:Data?){
        guard let data = data
        else{return}
        do{
            //let sstr = String(data: data, encoding: .utf8)!;print(sstr)
            self.sats = try JSONDecoder().decode([SchoolSATModel].self, from: data)
        }catch(let e){
            print(e)
        }
    }
    func getSchoolSAT(id:String)->SchoolSATModel?{
        return self.sats.filter{$0.id == id}.first
    }
    func loadMore(){
        print("load more!")
        //sleep(3)
    }
}
